extern crate ctftime_slackbot;
extern crate slack;

use ctftime_slackbot::handler;
use slack::RtmClient;
use std::{env, process};

fn main() {
    let api_key: String = api_key();
    let mut handler = handler::Handler;
    let r = RtmClient::login_and_run(&api_key, &mut handler);

    match r {
        Ok(_) => {},
        Err(err) => panic!("Error: {}", err)
    }
}

fn api_key() -> String {
    match env::var("SLACK_API_TOKEN") {
        Ok(val) => val,
        Err(_) => {
            println!("Environment variable `SLACK_API_TOKEN` is required");
            process::exit(1);
        }
    }
}

# CTFtime Slack Bot

## Setup
Setup is simple!

1. Add an app for your workspace
2. Add a bot user
3. Enable interactive components and set request URL to the server this app runs on.
4. Install app to your workspace
